## Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

Cuadro sinoptico de
[Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)

Dos expertos realizan una comparación sobre la evolución de la programación en las últimas dos décadas del pasado siglo y la actualidad.

**Fernando López** Ostenero profesor Departamento de Lenguajes y Sistemas Informáticos, UNED

**Antoni Burguera** Burguera informático, profesor, Universidad Islas Baleares

**Ana Isabel Ventureira** Pedrosa redactora - locutora, CEMAV, UNED
```plantuml

@startmindmap
header
29 de Septiembre del 2021
endheader
title Mapa conceptual del resumen sobre "Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .inicio {
    BackgroundColor lightblue
  }
}
</style>

* Programando ordenadores <<inicio>>

** Sistemas Viejos <<green>>
*** Ordenadores de los años 80s a 90s
*** Funcionaban sin actualizaciones a futuro
*** Lenguajes de Programacion
**** ALto nivel de programacion
***** Conocimiento del hardware
**** Conocimiento del hardware
***** Comunicacion directa con el ordenador


** Sistemas Modernos<<green>>
*** Poca variedad, Windows y Mac son mas comerciales
*** Lenguajes de Programacion alto nivel

** Programación Actual<<green>>
*** Lenguajes de programación han evoluacionado
**** Mejoras graduales
**** Multiplataforma
**** Actualizaciones y seguridad

*** Desventajas del ahora<<rose>>
**** Los compiladores no son tan eficiones como el ensamblador
**** Existe menos conocimiento del hardware

*** Ventajas del ahora<<rose>>
**** Las arquitecturas son mas publicas y hay más información
**** La velocidad de los ordenadores es más eficáz.
**** Precios accesibles

legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
@endmindmap
```
____

## Historia de los algoritmos y de los lenguajes de programación (2010)

Mapa mental de [La Historia de los algoritmos y de los lenguajes de programación](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)

De Euclides a Java. Historia de los algoritmos y de los lenguajes de programación", es el titulo de un libro escrito por Ricardo Peña Mari en el que se apoya la profesora Araujo para analizar la historia y alguno de los aspectos más importantes de la informática como los algoritmos y los lenguajes de programación.

**María Lourdes Araujo** Serna profesora Departamento de Lenguajes y Sistemas Informáticos, UNED

**Ricardo Peña Mari** profesor Departamento de Sistemas Informáticos y de la Computación, U.C.M.

**Ana Isabel Ventureira Pedrosa** redactora - locutora, CEMAV, UNED

```plantuml

@startmindmap
header
23 de Septiembre del 2021
endheader
title Historia de los algoritmos y de los lenguajes de programación (2010)
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .inicio {
    BackgroundColor #33DDFF
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }
 .red {
    BackgroundColor lightred
  }
}
</style>

* Algoritmos y lenguajes de programación<<inicio>>

** Algoritmos<<pink>>
***_ Son 
**** Lenguaje<<green>>
***** Para mostrar el proceso de forma escrita.
***** Resolver problemas
****** Como Manuales o instructivos


**** Tipos<<green>>
+++++ Razonables
****** El tiempo de ejecucion es despacio a \nmedida que el problema crece.
+++++ No razonables
****** El Tiempo de ejecucion crece a \nmedida que el problema crece

**** Caracteristicas<<green>>
***** Tienen inicio y fin
***** Funcionan en secuencia
***** Las secuencias son concretas
***** Los algoritmos son abstractos

**** Importantes<<green>>
***** Por que ayuda a las personas a resolver problemas
****** Matematicos, vida diaria, ammar cosas.

left
** Lenguajes de programacion<<pink>>

*** Lenguaje de computadora\n que los programadores <<green>>
****_ Utilizan para 
***** Comunicarse 
***** Desarrollar programas de software


*** Paradigmas<<inicio>>
****_ Los cuales
***** Es hacer referencia en caso de algo que se toma como modelo

****** Orientada a Objetos
******* Los objetos se utilizan como \nmetáfora para emular las entidades reales \ndel negocio a modelar.

****** Declarativa
******* Funcional
******** Basado en el uso de verdaderas funciones matemáticas.
******* Programación con restricciones
******** Las relaciones entre las variables son \nexpresadas en términos de restricciones.

****** Multiparadigma
******* emerge como resultado de la co-existencia de los paradigmas creados.

*** Programas<<inicio>>
**** Una secuencia de instrucciones
***** Escritas para realizar una tarea específica
***** Ejecutando las instrucciones del programa
****** En un procesador central.​ 

legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
@endmindmap
```
## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

Cuadro sinoptico de [Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc) 


**Anselmo Peñas Padilla** profesor del Departamento de Lenguajes y Sistemas Informáticos, UNED

**Javier Vélez Reyes** profesor del Departamento Lenguajes y Sistemas Informáticos, UNED

**Carlos Celorrio Aguilera** becario, UNED

**Emilio Julio Lorenzo Galgo** profesor del Departamento de Lenguajes y Sistemas Informáticos, UNED

**Ana Isabel Ventureira Pedrosa** redactora - locutora, CEMAV, UNED
```plantuml

@startmindmap
header
23 de Septiembre del 2021
endheader
title evolución de los lenguajes y paradigmas de programación
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .inicio {
    BackgroundColor #33DDFF
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }
 .red {
    BackgroundColor lightred
  }
}
</style>

* Evolución de los lenguajes y paradigmas de programación<<inicio>>

** Lenguajes de programación<<greencute>>
*** permite especificar qué datos debe operar un software específico 
****_ Java
****_ Phyton
****_ Javascript
****_ C
****_ C++

** Paradigma Logico<<greencute>>
*** Predicados logicos
**** Deja margen para la optimización
*** Modelizar un problema
**** Nivel de abstracción muy alto
*** Ejemplos<<pink>>
****_ Prolog
****_ Lisp
****_ Haskell
****_ Miranda
****_ Erlang

** Paradigma Funcional<<greencute>>
*** Basado en el uso de verdaderas funciones matemáticas
**** Las expresiones pueden ser asignadas a variables
****  Tiene sus raíces en el cálculo lambda
*** Recursividad
**** Optimizada mediante un compilador dentro del mismo código
**** Permiten la recursividad sin restricciones
**** Supera el test de Turing
*** Caracteristicas<<pink>>
****_ Programación declarativa
****_ Recursión
****_ Funciones como tipos de datos primitivos


** Paradigma Orientada a objetos<<greencute>>
*** Concepto de clases y objetos
**** Dejar de centrarnos en la lógica pura de los programas
**** Ayuda muchísimo en sistemas grandes
*** Ejemplos<<pink>>
****_ Java
*** Principios<<pink>>
****_ La encapsulación
****_ La abstracción
****_ La herencia
****_ El polimorfismo
*** Concebir el diseño de software
**** Reutilización del código
**** Estructuras simples reproducibles

** Paradigma Estructurado<<greencute>>
*** Analizar 
*** Basada en el teorema del programa estructurado
***_ diseñar los algoritmos utilizando
**** 3 estructuras básicas
**** Estructuras de control.


** Programacion distribuida<<greencute>>
*** Comunicar ordenadores muy distantes
*** Los programas de ejecutan en diversos programas<<pink>>
****_ Proyecto SETI
****_ Remedio contra el cáncer


** Programacion basada en componentes<<greencute>>
***_ Descomposición de sistemas 
**** Conformados en componentes funcionales o lógicos
*** Nivel mayor a la abstración
*** Descomposición de sistemas ya conformados en componentes

** Programacion multiagente o agente software<<greencute>>
*** Surge por la inteligencia artificial
**** Multiples agentes capaces de resolver objetivos
**** "Trabajo en equipo"
*** Aplicaciones autonomas que perciben su entorno
*** Resolver problemas complejos

** Abstracción <<greencute>>
*** Se enfoca en la visión externa de un objeto
**** Propiedades estáticas
***** El nombre, el tamaño, en algunas ocasiones su contenido.
**** Propiedades dinámicas
***** Peso, tamaño, contenido.
*** Tipos<<pink>>
****_ Abstracción de Acciones
****_ Abstracción de Máquinas virtuales
****_ Abstracción de Coincidencia
****_ Abstracción de Entidades

legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
@endmindmap
```